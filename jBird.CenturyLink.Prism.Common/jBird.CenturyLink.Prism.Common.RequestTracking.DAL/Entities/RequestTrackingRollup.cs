﻿using System;
using System.ComponentModel.DataAnnotations;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities
{
    public class RequestTrackingRollup
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public DateTime RollupDate { get; set; }

        [MaxLength(25)]
        [Required]
        public string RollupType { get; set; }

        [MaxLength(250)]
        [Required]
        public string ServerName { get; set; }

        [MaxLength(250)]
        [Required]
        public string ApplicationName { get; set; }

        [MaxLength(250)]
        [Required]
        public string MethodName { get; set; }

        [Required]
        public int NumberOfRequests { get; set; }

        [Required]
        public int NumberOfFailures { get; set; }

        [Required]
        public TimeSpan MedianRequestTime { get; set; }

        [Required]
        public TimeSpan MeanRequestTime { get; set; }

        [Required]
        public TimeSpan LongestRequestTime { get; set; }

        [Required]
        public TimeSpan ShortestRequestTime { get; set; }

    }
}
