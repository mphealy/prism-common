﻿using System.ComponentModel.DataAnnotations;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities
{
    public class UserRole
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
