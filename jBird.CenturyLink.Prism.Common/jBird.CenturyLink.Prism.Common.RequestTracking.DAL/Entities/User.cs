﻿using System;
using System.ComponentModel.DataAnnotations;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities
{
    public class User
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [MaxLength(150)]
        public string UserName { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime LastAccess { get; set; }

        public bool Inactive { get; set; }

        public bool Deleted { get; set; }

        [Required]
        public int UserRoleID { get; set; }
        
        public UserRole UserRole { get; set; }
    }
}
