﻿using System;
using System.ComponentModel.DataAnnotations;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities
{
    public class RequestTracking
    {
        [Required]
        public Guid ID { get; set; }

        [MaxLength(250)]
        [Required]
        public string ServerName { get; set; }

        [MaxLength(250)]
        [Required]
        public string ApplicationName { get; set; }

        public bool Successful { get; set; }

        public int ThreadID { get; set; }

        [MaxLength(250)]
        [Required]
        public string MethodName { get; set; }

        public string ErrorMessage { get; set; }

        public string RequestData { get; set; }

        public string ResponseData { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        [MaxLength(250)]
        public string AccountID { get; set; }

    }
}
