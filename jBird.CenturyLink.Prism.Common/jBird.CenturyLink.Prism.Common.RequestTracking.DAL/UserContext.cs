﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL
{
    public class UserContext : DbContext
    {
        public UserContext()
            : base("name=PrismanageTracking")
        {
            Database.SetInitializer<UserContext>(new UserInitializer());
        }
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
