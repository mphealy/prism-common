using jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class RequestTrackingContext : DbContext
    {
        // Your context has been configured to use a 'PrismanageTracking' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'jBird.CenturyLink.Prism.Common.RequestTracking.PrismanageTracking' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'PrismanageTracking' 
        // connection string in the application configuration file.
        public RequestTrackingContext()
            : base("name=PrismanageTracking")
        {
            Database.SetInitializer<RequestTrackingContext>(new RequestTrackingInitializer());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Entities.RequestTracking> RequestTrackings { get; set; }

        public virtual DbSet<RequestTrackingRollup> RequestTrackingRollups { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

}