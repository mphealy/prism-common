﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DAL
{
    public class RequestTrackingInitializer : CreateDatabaseIfNotExists<RequestTrackingContext>
    {
        protected override void Seed(RequestTrackingContext context)
        {
            base.Seed(context);
        }
    }
    public class UserInitializer : CreateDatabaseIfNotExists<UserContext>
    {
        protected override void Seed(UserContext context)
        {
            context.UserRoles.Add(new UserRole() { Name = "Admin" });
            context.UserRoles.Add(new UserRole() { Name = "Update" });
            context.UserRoles.Add(new UserRole() { Name = "Read" });
            base.Seed(context);
        }
    }
}
