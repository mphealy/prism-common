﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common.Helpers;
using Newtonsoft.Json;
using ActionFilterAttribute = System.Web.Mvc.ActionFilterAttribute;
using IExceptionFilter = System.Web.Mvc.IExceptionFilter;

namespace jBird.CenturyLink.Prism.Common.RequestTracking
{
    public class RequestTrackerAttribute : ActionFilterAttribute, IExceptionFilter
    {
        const string RequestTrackerKey = "RequestTrackerObject";
        const string ApplicationNameKey = "applicationName";
        const string AccountIDKey = "MediaRoomAccountId";
        bool storeResponseObject = Convert.ToBoolean(ConfigurationManager.AppSettings["storeResponseObject"]);
        bool storeRequestObject = Convert.ToBoolean(ConfigurationManager.AppSettings["storeRequestObject"]);
        static readonly IRequestTrackingService requestTrackingService = new RequestTrackingService();

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            var track = new DTO.RequestTracking()
            {
                ID = Guid.NewGuid(),
                ServerName = Environment.MachineName,
                StartTime = DateTime.UtcNow,
                ThreadID = Thread.CurrentThread.ManagedThreadId,
                MethodName = actionContext.HttpContext.Request.Url?.AbsoluteUri ?? ""
            };

            track.ApplicationName = actionContext.HttpContext.Request.QueryString[ApplicationNameKey] ?? "";
            track.AccountID = actionContext.HttpContext.Request.Form[AccountIDKey] ?? "";
            actionContext.HttpContext.Request.ServerVariables[RequestTrackerKey] = JsonConvert.SerializeObject(track);
        }
        public override void OnActionExecuted(ActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            var track =
                JsonConvert.DeserializeObject<DTO.RequestTracking>(actionExecutedContext.HttpContext.Request.ServerVariables[RequestTrackerKey]);

            track.EndTime = DateTime.UtcNow;
            track.Successful = true;
            track.RequestData = JsonConvert.SerializeObject(
                        new
                        {
                            RequestUri = actionExecutedContext.HttpContext.Request.Url?.AbsoluteUri ?? "",
                            Headers = actionExecutedContext.HttpContext.Request.Headers.ToDictionary(),
                            Method = actionExecutedContext.HttpContext.Request.HttpMethod,
                            Content = actionExecutedContext.HttpContext.Request.Form.ToDictionary()
                        }, Formatting.Indented,
                        new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            if (actionExecutedContext.HttpContext.Response != null && storeResponseObject)
            {
                track.ResponseData = JsonConvert.SerializeObject(
                    new
                    {
                        actionExecutedContext.HttpContext.Response.StatusCode,
                        Headers = actionExecutedContext.HttpContext.Response.Headers.ToDictionary(),
                        actionExecutedContext.HttpContext.Response.Cookies,
                        actionExecutedContext.HttpContext.Response.Cache
                    }, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            }

            requestTrackingService.Create(track);
        }

        public void OnException(ExceptionContext filterContext)
        {
            var track = new DTO.RequestTracking()
            {
                ApplicationName = System.Diagnostics.Process.GetCurrentProcess().ProcessName,
                ID = Guid.NewGuid(),
                ServerName = Environment.MachineName,
                StartTime = DateTime.Now,
                ThreadID = Thread.CurrentThread.ManagedThreadId,
                EndTime = DateTime.Now,
                Successful = false,
                MethodName = filterContext.HttpContext.Request.Url?.AbsoluteUri ?? "",
                ErrorMessage = filterContext.Exception.Message
            };

            if (storeRequestObject)
            {
                track.RequestData =
                    JsonConvert.SerializeObject(
                        new
                        {
                            RequestUri = filterContext.HttpContext.Request.Url?.AbsoluteUri ?? "",
                            Headers = filterContext.HttpContext.Request.Headers.ToDictionary(),
                            Method = filterContext.HttpContext.Request.HttpMethod,
                            Content = filterContext.HttpContext.Request.Form.ToDictionary()
                        }, Formatting.Indented,
                        new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            }

            if (filterContext.HttpContext.Response != null && storeResponseObject)
            {
                track.ResponseData = JsonConvert.SerializeObject(
                    new
                    {
                        filterContext.HttpContext.Response.StatusCode,
                        Headers = filterContext.HttpContext.Response.Headers.ToDictionary(),
                        filterContext.HttpContext.Response.Cookies,
                        filterContext.HttpContext.Response.Cache
                    }, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            }

            requestTrackingService.Create(track);
        }
    }
}
