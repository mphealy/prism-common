﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services;
using Newtonsoft.Json;

namespace jBird.CenturyLink.Prism.Common.RequestTracking
{
    public class RequestTrackerApiAttribute : ActionFilterAttribute, IExceptionFilter
    {
        const string RequestTrackerKey = "RequestTrackerObject";
        const string ApplicationNameKey = "applicationName";
        bool storeResponseObject = Convert.ToBoolean(ConfigurationManager.AppSettings["storeResponseObject"]);
        bool storeRequestObject = Convert.ToBoolean(ConfigurationManager.AppSettings["storeRequestObject"]);
        static readonly IRequestTrackingService requestTrackingService = new RequestTrackingService();

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            var track = new DTO.RequestTracking
            {
                ID = Guid.NewGuid(),
                ServerName = Environment.MachineName,
                StartTime = DateTime.UtcNow,
                ThreadID = Thread.CurrentThread.ManagedThreadId,
                MethodName = actionContext.Request.RequestUri.AbsoluteUri
            };

            var deserializedObj =
                JsonConvert.DeserializeObject<DeserializedResponse>(actionContext.Request.Content.ReadAsStringAsync().Result);
            var queryString = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);

            track.ApplicationName = queryString[ApplicationNameKey] ?? "";
            track.AccountID = deserializedObj.MediaRoomAccountId ?? "";
            actionContext.Request.Properties[RequestTrackerKey] = track;
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            var track = actionExecutedContext.Request.Properties[RequestTrackerKey] as DTO.RequestTracking;

            if (track == null) return;

            track.EndTime = DateTime.UtcNow;
            track.Successful = true;
            track.RequestData = JsonConvert.SerializeObject(
                            new
                            {
                                actionExecutedContext.Request.RequestUri,
                                actionExecutedContext.Request.Headers,
                                actionExecutedContext.Request.Method,
                                Content = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionExecutedContext.Request.Content.ReadAsStringAsync().Result, new NestedJsonConverter())
                            }, Formatting.Indented,
                            new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            if (actionExecutedContext.Response != null && storeResponseObject)
            {
                track.ResponseData = JsonConvert.SerializeObject(
                    new
                    {
                        actionExecutedContext.Response.StatusCode,
                        actionExecutedContext.Response.Headers,
                        Content = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionExecutedContext.Response.Content.ReadAsStringAsync().Result, new NestedJsonConverter())
                    }, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            }

            requestTrackingService.Create(track);
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext,
            CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() =>
            {
                var track = new DTO.RequestTracking()
                {
                    ApplicationName = System.Diagnostics.Process.GetCurrentProcess().ProcessName,
                    ID = Guid.NewGuid(),
                    ServerName = Environment.MachineName,
                    StartTime = DateTime.Now,
                    ThreadID = Thread.CurrentThread.ManagedThreadId,
                    EndTime = DateTime.Now,
                    Successful = false,
                    MethodName = actionExecutedContext.Request.RequestUri.AbsoluteUri,
                    ErrorMessage = actionExecutedContext.Exception.Message
                };

                if (storeRequestObject)
                {
                    track.RequestData =
                        JsonConvert.SerializeObject(
                            new
                            {
                                actionExecutedContext.Request.RequestUri,
                                actionExecutedContext.Request.Headers,
                                actionExecutedContext.Request.Method,
                                Content = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionExecutedContext.Request.Content.ReadAsStringAsync().Result, new NestedJsonConverter())
                            }, Formatting.Indented,
                            new JsonSerializerSettings {PreserveReferencesHandling = PreserveReferencesHandling.Objects});
                }

                if (actionExecutedContext.Response != null && storeResponseObject)
                {
                    track.ResponseData = JsonConvert.SerializeObject(
                        new
                        {
                            actionExecutedContext.Response.StatusCode,
                            actionExecutedContext.Response.Headers,
                            Content = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionExecutedContext.Response.Content.ReadAsStringAsync().Result, new NestedJsonConverter())
                        }, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                }

                requestTrackingService.Create(track);
            }, cancellationToken);
        }
    }
}
