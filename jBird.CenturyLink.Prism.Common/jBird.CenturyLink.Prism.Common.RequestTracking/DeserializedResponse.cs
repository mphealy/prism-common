﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jBird.CenturyLink.Prism.Common.RequestTracking
{
    internal class DeserializedResponse
    {
        public string MediaRoomAccountId { get; set; }
    }
}
