﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common;
using jBird.CenturyLink.Prism.Common.RequestTracking.DAL.Entities;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Bll
{
    public static class ConvertionExtensions
    {
        #region User
        public static User ToEntity(this DTO.User user)
        {
            var entity = new User
            {
                ID = user.ID,
                UserName = user.UserName,
                DateCreated = user.DateCreated,
                Deleted = user.Deleted,
                Inactive = user.Inactive,
                UserRoleID = (int)user.UserRole,
                LastAccess = user.LastAccess
            };

            return entity;
        }

        public static DTO.User ToDTO(this User user, int timezoneOffset)
        {
            var obj = new DTO.User
            {
                ID = user.ID,
                UserName = user.UserName,
                DateCreated = user.DateCreated,
                Deleted = user.Deleted,
                Inactive = user.Inactive,
                UserRole = (UserRoles)user.UserRoleID,
                LastAccess = user.LastAccess
            };

            obj = ToClientTime<DTO.User>(obj, timezoneOffset);
            return obj;
        }
        #endregion

        #region UserRole

        public static UserRole ToEntity(this DTO.UserRole userRole)
        {
            return new UserRole
            {
                ID = userRole.ID,
                Name = userRole.Name
            };
        }

        public static DTO.UserRole ToDTO(this UserRole userRole)
        {
            return new DTO.UserRole
            {
                ID = userRole.ID,
                Name = userRole.Name
            };
        }
        #endregion

        #region RequestTracking

        public static DAL.Entities.RequestTracking ToEntity(this DTO.RequestTracking requestTracking)
        {
            var entity = new DAL.Entities.RequestTracking
            {
                ID = requestTracking.ID,
                ServerName = requestTracking.ServerName,
                ApplicationName = requestTracking.ApplicationName,
                Successful = requestTracking.Successful,
                ThreadID = requestTracking.ThreadID,
                MethodName = requestTracking.MethodName,
                ErrorMessage = requestTracking.ErrorMessage,
                RequestData = requestTracking.RequestData,
                ResponseData = requestTracking.ResponseData,
                StartTime = requestTracking.StartTime,
                EndTime = requestTracking.EndTime,
                AccountID = requestTracking.AccountID
            };

            return entity;
        }

        public static DTO.RequestTracking ToDTO(this DAL.Entities.RequestTracking requestTracking, int timezoneOffset)
        {
            var obj = new DTO.RequestTracking
            {
                ID = requestTracking.ID,
                ServerName = requestTracking.ServerName,
                ApplicationName = requestTracking.ApplicationName,
                Successful = requestTracking.Successful,
                ThreadID = requestTracking.ThreadID,
                MethodName = requestTracking.MethodName,
                ErrorMessage = requestTracking.ErrorMessage,
                RequestData = requestTracking.RequestData,
                ResponseData = requestTracking.ResponseData,
                StartTime = requestTracking.StartTime,
                EndTime = requestTracking.EndTime,
                AccountID = requestTracking.AccountID,
                RequestTime = requestTracking.EndTime - requestTracking.StartTime
            };

            obj = ToClientTime<DTO.RequestTracking>(obj, timezoneOffset);
            return obj;
        }
        #endregion

        #region RequetTrackingRollup

        public static RequestTrackingRollup ToEntity(this DTO.RequestTrackingRollup requestTrackingRollup)
        {
            var entity = new RequestTrackingRollup
            {
                ID = requestTrackingRollup.ID,
                RollupType = requestTrackingRollup.RollupType,
                ServerName = requestTrackingRollup.ServerName,
                ApplicationName = requestTrackingRollup.ApplicationName,
                MethodName = requestTrackingRollup.MethodName,
                NumberOfRequests = requestTrackingRollup.NumberOfRequests,
                NumberOfFailures = requestTrackingRollup.NumberOfFailures,
                MedianRequestTime = requestTrackingRollup.MedianRequestTime,
                MeanRequestTime = requestTrackingRollup.MeanRequestTime,
                LongestRequestTime = requestTrackingRollup.LongestRequestTime,
                ShortestRequestTime = requestTrackingRollup.ShortestRequestTime,
                RollupDate = requestTrackingRollup.RollupDate
            };

            return entity;
        }

        public static DTO.RequestTrackingRollup ToDTO(this RequestTrackingRollup requestTrackingRollup, int timezoneOffset)
        {
            var obj = new DTO.RequestTrackingRollup
            {
                ID = requestTrackingRollup.ID,
                RollupType = requestTrackingRollup.RollupType,
                ServerName = requestTrackingRollup.ServerName,
                ApplicationName = requestTrackingRollup.ApplicationName,
                MethodName = requestTrackingRollup.MethodName,
                NumberOfRequests = requestTrackingRollup.NumberOfRequests,
                NumberOfFailures = requestTrackingRollup.NumberOfFailures,
                MedianRequestTime = requestTrackingRollup.MedianRequestTime,
                MeanRequestTime = requestTrackingRollup.MeanRequestTime,
                LongestRequestTime = requestTrackingRollup.LongestRequestTime,
                ShortestRequestTime = requestTrackingRollup.ShortestRequestTime,
                RollupDate = requestTrackingRollup.RollupDate
            };

            obj = ToClientTime<DTO.RequestTrackingRollup>(obj, timezoneOffset);
            return obj;
        }
        #endregion

        public static T ToClientTime<T>(object item, int timezoneOffset)
        {
            var myType = typeof(T);

            var fields = myType.GetFields(BindingFlags.Instance |
                                           BindingFlags.Static |
                                           BindingFlags.NonPublic |
                                           BindingFlags.Public);

            foreach (var field in fields)
            {
                if (field.FieldType == typeof(DateTime))
                {
                    var dateTimeUtc = (DateTime)field.GetValue(item);
                    if (dateTimeUtc != DateTime.MinValue)
                    {
                        dateTimeUtc = dateTimeUtc.AddMinutes(-1 * timezoneOffset);
                        field.SetValue(item, dateTimeUtc);
                    }
                }
            }
            return (T)item;
        }
    }
}
