﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services
{
    public interface IRequestTrackingService
    {
        Guid Create(DTO.RequestTracking requestTracking);

        DTO.RequestTrackingFilteredModel GetByApplicationForToday(string applicationName, int timezoneOffset, RequestSearchFields? field, OrderType? order, int? page = null, int? countByPage = null);

        DTO.RequestTracking GetById(Guid id, int timezoneOffset);

        IEnumerable<string> GetAllApplications();
    }
}
