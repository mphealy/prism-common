﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.DTO;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services
{
    public interface IUserService
    {
        User GetUserById(int id, int timezoneOffset);

        User GetUserByName(string username, int timezoneOffset);

        void UpdateLastAccessTime(int userId);

        IEnumerable<User> GetAllUsers(int timezoneOffset);

        int UpdateUser(User user);

        int CreateUser(User user);
    }
}
