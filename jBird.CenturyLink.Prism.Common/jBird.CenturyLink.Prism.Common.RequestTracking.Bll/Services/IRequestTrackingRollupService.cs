﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jBird.CenturyLink.Prism.Common.RequestTracking.DTO;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services
{
    public interface IRequestTrackingRollupService
    {
        IEnumerable<RequestTrackingRollup> GetLastRollups(int count, int timezoneOffset);

        IEnumerable<RequestTrackingRollup> GetRollupsByType(string type, DateTime dateFrom, DateTime dateTo, int timezoneOffset);
    }
}
