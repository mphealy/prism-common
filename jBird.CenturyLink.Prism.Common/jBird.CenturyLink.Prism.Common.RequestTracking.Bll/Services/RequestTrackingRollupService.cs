﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services;
using jBird.CenturyLink.Prism.Common.RequestTracking.DAL;
using jBird.CenturyLink.Prism.Common.RequestTracking.DTO;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services
{
    public class RequestTrackingRollupService : BaseService, IRequestTrackingRollupService
    {
        public IEnumerable<RequestTrackingRollup> GetLastRollups(int count, int timezoneOffset)
        {
            var lastRollups = new List<RequestTrackingRollup>();
            var daysBefore = DateTime.UtcNow.AddDays(-count);
            var dateFrom = new DateTime(daysBefore.Year, daysBefore.Month, daysBefore.Day, 0, 0, 0);

            try
            {
                using (var context = new RequestTrackingContext())
                {
                    context.RequestTrackingRollups.AsEnumerable().Where(g => g.RollupDate >= dateFrom).OrderByDescending(g => g.RollupDate).ToList().ForEach(g => lastRollups.Add(g.ToDTO(timezoneOffset)));
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error occured in RequestTrackingRollupService in GetLastRollups method. Count: {0}. Exception: {1}", count, ex);
            }

            return lastRollups;
        }

        public IEnumerable<RequestTrackingRollup> GetRollupsByType(string type, DateTime dateFrom, DateTime dateTo, int timezoneOffset)
        {
            var rollups = new List<RequestTrackingRollup>();

            try
            {
                if (string.IsNullOrEmpty(type))
                {
                    throw new ArgumentNullException("Rollup Type is not defined");
                }

                using (var context = new RequestTrackingContext())
                {
                    context.RequestTrackingRollups.Where(g => g.RollupType == type && g.RollupDate >= dateFrom && g.RollupDate <= dateTo).ToList().ForEach(g => rollups.Add(g.ToDTO(timezoneOffset)));
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error occured in RequestTrackingRollupService in GetRollupsByType method. Type: {0}, DateFrom: {1}, DateTo {2}, TimezoneOffset {3}. Exception: {4}", type, dateFrom.ToString("yyyy-MM-dd HH:mm:ss"), dateTo.ToString("yyyy-MM-dd HH:mm:ss"), timezoneOffset, ex);
            }

            return rollups;
        }
    }
}
