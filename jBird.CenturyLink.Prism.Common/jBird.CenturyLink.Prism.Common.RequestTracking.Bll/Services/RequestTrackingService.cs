﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common.Exceptions;
using jBird.CenturyLink.Prism.Common.RequestTracking.DAL;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common.Helpers;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Bll.Services
{
    public class RequestTrackingService : BaseService, IRequestTrackingService
    {
        public Guid Create(DTO.RequestTracking requestTracking)
        {
            try
            {
                using (var context = new RequestTrackingContext())
                {
                    var entity = context.RequestTrackings.Add(requestTracking.ToEntity());
                    context.SaveChanges();
                    return entity.ID;
                }
            }
            catch (DbEntityValidationException ex)
            {
                var entityValidationErrors = "";
                if (ex.EntityValidationErrors != null)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        entityValidationErrors += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            entityValidationErrors += string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                    ve.PropertyName,
                                    eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                    ve.ErrorMessage);
                        }
                    }
                }
                Logger.ErrorFormat($"Error occured in RequestTrackingService class in Create method. Exception: {ex}. EntityValidationErrors: {entityValidationErrors}");
                return Guid.Empty;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"Error occured in RequestTrackingService class in Create method. Exception: {ex}");
                return Guid.Empty;
            }
        }

        public DTO.RequestTrackingFilteredModel GetByApplicationForToday(string applicationName, int timezoneOffset, RequestSearchFields? field = RequestSearchFields.EndTime, OrderType? order = OrderType.Desc, int? page = null, int? countByPage = null)
        {
            var model = new DTO.RequestTrackingFilteredModel();
            var now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            var endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            var skip = 0;
            var dtoList = new List<DTO.RequestTracking>();
            
            try
            {
                using (var context = new RequestTrackingContext())
                {
                    context.RequestTrackings.AsEnumerable().Where(g => g.ApplicationName == applicationName && g.EndTime >= startDate && g.EndTime <= endDate).OrderByDescending(g => g.EndTime).ToList().ForEach(g => dtoList.Add(g.ToDTO(timezoneOffset)));
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"Error occured in RequestTrackingService class in GetAll method. Exception: {ex}");
            }

            model.TotalRequests = dtoList.Count;
            model.TotalSucceeded = dtoList.Count(g => g.Successful);
            model.TotalFailed = dtoList.Count(g => !g.Successful);

            Expression<Func<DTO.RequestTracking, object>> orderByField = null;

            switch (field)
            {
                case RequestSearchFields.ServerName:
                    orderByField = (g) => g.ServerName; 
                    break;
                case RequestSearchFields.MethodName:
                    orderByField = (g) => g.MethodName;
                    break;
                case RequestSearchFields.ErrorMessage:
                    orderByField = (g) => g.ErrorMessage;
                    break;
                case RequestSearchFields.StartTime:
                    orderByField = (g) => g.StartTime;
                    break;
                case RequestSearchFields.EndTime:
                    orderByField = (g) => g.EndTime;
                    break;
                case RequestSearchFields.Successful:
                    orderByField = (g) => g.Successful;
                    break;
            }

            if (page != null && countByPage != null)
            {
                skip = (int)page * (int)countByPage;
                model.FilteredRequestTrackingList = dtoList.AsQueryable().OrderBy(orderByField, (OrderType)order).Skip(skip).Take(countByPage.Value).ToList();
            }
            else
            {
                model.FilteredRequestTrackingList = dtoList;
            }

            return model;
        }

        public DTO.RequestTracking GetById(Guid id, int timezoneOffset)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    throw new ArgumentNullException("Guid is empty");
                }

                using (var context = new RequestTrackingContext())
                {
                    var foundItem = context.RequestTrackings.AsEnumerable().FirstOrDefault(g => g.ID == id);

                    if (foundItem == null)
                    {
                        throw new EntityNotFoundException($"Request Tracking item not found by guid {id}");
                    }

                    return foundItem.ToDTO(timezoneOffset);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"Error occured in RequestTrackingService class in GetById method. Id: {id}. Exception: {ex}.");
                return null;
            }
        }

        public IEnumerable<string> GetAllApplications()
        {
            var applicationList = new List<string>();

            try
            {
                using (var context = new RequestTrackingContext())
                {
                    applicationList = context.RequestTrackings.AsEnumerable().Select(g => g.ApplicationName).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"Error occured in RequestTrackingService class in GetAllApplications method. Exception: {ex}.");
            }

            return applicationList;
        }
    }
}
