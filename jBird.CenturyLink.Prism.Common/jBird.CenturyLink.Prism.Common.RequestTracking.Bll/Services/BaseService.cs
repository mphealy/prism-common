﻿using log4net;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services
{
    public class BaseService
    {
        protected ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
