﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common.Exceptions;
using jBird.CenturyLink.Prism.Common.RequestTracking.DTO;
using jBird.CenturyLink.Prism.Common.RequestTracking.Bll;
using UserContext = jBird.CenturyLink.Prism.Common.RequestTracking.DAL.UserContext;
using log4net;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public User GetUserById(int id, int timezoneOffset)
        {
            try
            {
                using (var context = new UserContext())
                {
                    var user = context.Users.AsEnumerable().FirstOrDefault(g => g.ID == id);

                    if (user == null)
                    {
                        throw new EntityNotFoundException($"User not found with id {id}");
                    }

                    return user.ToDTO(timezoneOffset);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in GetUserById method. Id: {0}. Exception: {1}", id, ex);
                return null;
            }
        }

        public User GetUserByName(string username, int timezoneOffset)
        {
            try
            {
                using (var context = new UserContext())
                {
                    var foundUser = context.Users.AsEnumerable().FirstOrDefault(g => g.UserName == username);

                    if (foundUser == null)
                    {
                        throw new EntityNotFoundException($"User not found by username {username}");
                    }

                    return foundUser.ToDTO(timezoneOffset);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in GetUserByName method. UserName: {0}. Exception: {1}", username, ex);
                return null;
            }
        }

        public void UpdateLastAccessTime(int userId)
        {
            try
            {
                using (var context = new UserContext())
                {
                    var foundUser = context.Users.AsEnumerable().FirstOrDefault(g => g.ID == userId);

                    if (foundUser == null)
                    {
                        throw new EntityNotFoundException($"User not found by id {userId}");
                    }

                    foundUser.LastAccess = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in UpdateLastAccessTime method. UserId: {0}. Exception: {1}", userId, ex);
            }
        }

        public IEnumerable<User> GetAllUsers(int timezoneOffset)
        {
            var userList = new List<User>();

            try
            {
                using (var context = new UserContext())
                {
                    context.Users.AsEnumerable().ToList().ForEach(g => userList.Add(g.ToDTO(timezoneOffset)));
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in GetAllUsers method. Exception: {0}", ex);
            }

            return userList;
        }

        public int UpdateUser(User user)
        {
            try
            {
                using (var context = new UserContext())
                {
                    if (user.ID == 0)
                    {
                        throw new InvalidEntityException("User id is empty");
                    }

                    var entity = user.ToEntity();

                    context.Users.Attach(entity);
                    context.Entry(entity).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in UpdateUser method. Id: {0}, UserName: {1}. Exception: {2}", user.ID, user.UserName, ex);
            }

            return user.ID;
        }

        public int CreateUser(User user)
        {
            try
            {
                using (var context = new UserContext())
                {
                    var createdUser = context.Users.Add(user.ToEntity());
                    context.SaveChanges();

                    return createdUser.ID;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("An error occured in UserService in CreateUser method. UserName: {0}. Exception: {1}", user.UserName, ex);
                return 0;
            }
        }
    }
}
