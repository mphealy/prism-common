﻿using System;
using System.Collections.Specialized;
using System.Linq;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Common.Helpers
{
    public static class Extensions
    {
        public static object ToDictionary(this NameValueCollection source)
        {
            return source.Cast<string>().ToDictionary(item => item, item => source[item]);
        }

        public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source,
            System.Linq.Expressions.Expression<Func<TSource, TKey>> keySelector, OrderType orderType)
        {
            return orderType == OrderType.Asc ? source.OrderBy(keySelector) : source.OrderByDescending(keySelector);
        }
    }
}
