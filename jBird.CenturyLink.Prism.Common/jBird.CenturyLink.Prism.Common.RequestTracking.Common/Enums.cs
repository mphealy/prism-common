﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Common
{
    public enum UserRoles
    {
        [Description("Admin")]
        Admin = 1,
        [Description("Update")]
        Update,
        [Description("Read")]
        Read
    }

    public enum RequestSearchFields
    {
        ServerName,
        MethodName,
        Successful,
        ErrorMessage,
        StartTime,
        EndTime
    }

    public enum OrderType
    {
        Asc,
        Desc
    }
}
