﻿using System;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.Common.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message) : base(message)
        {
            
        }
    }
}
