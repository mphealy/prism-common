﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DTO
{
    [Serializable]
    [DataContract]
    public class RequestTracking
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string ServerName { get; set; }

        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public bool Successful { get; set; }

        [DataMember]
        public int ThreadID { get; set; }

        [DataMember]
        public string MethodName { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public string RequestData { get; set; }

        [DataMember]
        public string ResponseData { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public string AccountID { get; set; }

        [DataMember]
        public TimeSpan RequestTime { get; set; }
    }
}
