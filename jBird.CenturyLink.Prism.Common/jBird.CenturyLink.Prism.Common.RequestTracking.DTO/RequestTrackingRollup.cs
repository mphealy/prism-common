﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DTO
{
    [Serializable]
    [DataContract]
    public class RequestTrackingRollup
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public DateTime RollupDate { get; set; }

        [DataMember]
        public string RollupType { get; set; }

        [DataMember]
        public string ServerName { get; set; }

        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public string MethodName { get; set; }

        [DataMember]
        public int NumberOfRequests { get; set; }

        [DataMember]
        public int NumberOfFailures { get; set; }

        [DataMember]
        public TimeSpan MedianRequestTime { get; set; }

        [DataMember]
        public TimeSpan MeanRequestTime { get; set; }

        [DataMember]
        public TimeSpan LongestRequestTime { get; set; }

        [DataMember]
        public TimeSpan ShortestRequestTime { get; set; }
    }
}
