﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DTO
{
    public class RequestTrackingFilteredModel
    {
        public List<RequestTracking> FilteredRequestTrackingList { get; set; }
        public int TotalRequests { get; set; }
        public int TotalSucceeded { get; set; }
        public int TotalFailed { get; set; }

        public RequestTrackingFilteredModel()
        {
            FilteredRequestTrackingList = new List<RequestTracking>();
        }
    }
}
