﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DTO
{
    [DataContract]
    [Serializable]
    public class UserRole
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
