﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using jBird.CenturyLink.Prism.Common.RequestTracking.Common;

namespace jBird.CenturyLink.Prism.Common.RequestTracking.DTO
{
    [Serializable]
    [DataContract]
    public class User
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime LastAccess { get; set; }

        [DataMember]
        public bool Inactive { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public UserRoles UserRole { get; set; }
    }
}
