﻿using jBird.CenturyLink.Prism.Common.RequestTracking.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTrackingDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new RequestTrackingContext())
            {
                var track = db.RequestTrackings.FirstOrDefault(a => a.AccountID == "1234567890");
                var rollup = db.RequestTrackingRollups.FirstOrDefault(r => r.ID == 1);

            }
            using (var db = new UserContext())
            {
                var user = db.Users.FirstOrDefault(u => u.UserName == "jaybird");
                var userRole = db.UserRoles.FirstOrDefault(ur => ur.Name == "admin");

            }
            Console.WriteLine("Completed; Press any key to continue.");
            Console.ReadLine();
        }
    }
}
