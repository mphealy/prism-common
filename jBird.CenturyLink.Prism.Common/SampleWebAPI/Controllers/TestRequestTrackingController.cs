﻿using jBird.CenturyLink.Prism.Common.RequestTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SampleWebAPI.Controllers
{
    public class TestRequestTrackingController : ApiController
    {
        [AcceptVerbs("POST")]
        [RequestTrackerApi]
        public string TestTracker()
        {
            return "Hello WOrld";
        }
    }
}
