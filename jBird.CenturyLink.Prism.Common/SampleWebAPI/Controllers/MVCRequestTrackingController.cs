﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jBird.CenturyLink.Prism.Common.RequestTracking;
using jBird.CenturyLink.Prism.Common.RequestTracking.BLL.Services;

namespace SampleWebAPI.Controllers
{
    public class MVCRequestTrackingController : Controller
    {
        // GET: MVCRequestTracking
        [HttpPost]
        [RequestTracker]
        public ActionResult Index()
        {
            return View();
        }
    }
}